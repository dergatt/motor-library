This library controls a stepper motor.
This is my first library and it will grow with my projects to fit my needs.


## Funtions

**motor(**int** pin1, **int** pin2, **int** pin3, **int** pin4, [**int** steps for full rotation], **int** speed [**bool** should half steps be used])**
initialize

**rotate(steps)**
rotates the motor

**fullrotation(times)**
rotates by 360 degres

**angle(degree)**
rotate for an angle

**setclockwise(bool)**
sets if rotating clock or counterclockwise

**setspeed(int)**
Higher number means solwer rotation. Default is 2. If number is lower than 2 it is set to 2.

### motor
Initialize the motor. Motor pins are required. Steps to full rotation is optional and defaults to 2048 speed defaults to 2. should half steps be used is optional defaults to false.

### rotate
Takes int as input. Rotates the motor for x steps.

### fullrotation
Takes int as input. Rotates the motor for 360 degrees x times. Defaults to 1.
Only works if steps for full rotation is set correctly.

### angle
Takes int as input. Rotates the motor from current position to the set angle.
Only works if steps for full rotation is set correctly.

### clockwise
Sets if the motor rotates clock or counterclockwise. This is set by a bool variable.

### setspeed
Sets the pause between each step. A higher number means a longer pause and slower rotation. The minimum and default is set to 2. Otherwies the stepper can't keep up with the speed.

## Example

```arduino
//initialize the motor with full steps
Motor motor(1, 2, 3, 4, 2048);

//initialize the motor with half steps
Motor motor(1, 2, 3, 4, 4096, true);

//initialize the motor with half the speed
Motor motor(1, 2, 3, 4, 2048, 4);

//rotate the motor for 200 steps
motor.rotate(200);

//3 times a full rotation
motor.fullrotation(3);

//rotates by 90°
motor.angle(90);

//set motor to rotate counterclockwise
motor.setclockwise(false);

//set the the speed back to default value
motor.setspeed();

//set motor speed to half the top speed
motor.setspeed(4);
```
