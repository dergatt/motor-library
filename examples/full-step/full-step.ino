/*
 * 
 * 
 * This is an example shows how to use the motor library in full step mode.
 * This mode is faster and more powerful than half step mode but less precise.
 *
 */




#include<Motor.h>

// set configuration variables
//set the pins the motor is connected
int pin1 = 27; 
int pin2 = 14; 
int pin3 = 12; 
int pin4 = 13;
//Motor settings how many steps before a full rotation is reached
int full_rotation = 2048;



//Initialize the motor
Motor motor(pin1, pin2, pin3, pin4, full_rotation);

void setup () { 

}


void loop() {


//rotate the motor for 200 steps
motor.rotate(200);

delay(1000);

//3 times a full rotation
motor.fullrotation(3);

delay(1000);

//rotates by 90°
motor.angle(90);

//set motor speed to half the top speed
motor.setspeed(4);

delay(1000);

//rotate 1 time
motor.rotate(1);

//set motor to rotate counterclockwise
motor.setclockwise(false); 

//set the the speed back to top speed
motor.setspeed();


}
