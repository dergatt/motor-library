#include "Arduino.h"
#include "Motor.h"

Motor::Motor(int pin1, int pin2, int pin3, int pin4, int completerotation, int speed, bool halfstep) {
	int pins[] = {pin1, pin2, pin3, pin4};
	for(int x = 0; x < 4; x++) {
		_pins[x] = pins[x];
	}
	
	_completerotation = completerotation;
	_count = 0;
	_pinnumber = 4;
	_clockwise = true;
	_anglehelper = float(_completerotation) / 360;
	_speed = speed;

	int full_steps[][4] = {
		{HIGH, HIGH, LOW, LOW},
		{LOW, HIGH, HIGH, LOW},
		{LOW, LOW, HIGH, HIGH},
		{HIGH, LOW, LOW, HIGH}
	};

	int half_steps[][4] = {
		{HIGH, LOW, LOW, LOW},
		{HIGH, HIGH, LOW, LOW},
		{LOW, HIGH, LOW, LOW},
		{LOW, HIGH, HIGH, LOW},
		{LOW, LOW, HIGH, LOW},
		{LOW, LOW, HIGH, HIGH},
		{LOW, LOW, LOW, HIGH},
		{HIGH, LOW, LOW, HIGH}
	};


	 if(halfstep) {
		_stepnumber = 7;
		for(int x = 0; x <= _stepnumber; x++) {
			for( int y = 0; y < _pinnumber; y++){
				_stepsequence[x][y] = half_steps[x][y];
			}
		}
	 } else {
		_stepnumber = 3;
		for(int x = 0; x <= _stepnumber; x++) {
			for( int y = 0; y < _pinnumber; y++){
				_stepsequence[x][y] = full_steps[x][y];
			}
		}
	 }


	for(int pin=0; pin < _pinnumber; pin++) {
		pinMode(_pins[pin], OUTPUT);
		digitalWrite(_pins[pin],LOW);
	}

}

void Motor::rotate(int target) {
	int clicks = 0;
	while (clicks < target) {
		if(_clockwise) {
			if (_count == _stepnumber) {
				_count = 0;
			} else {
				_count++;
			}
		} else {
			if (_count == 0) {
				_count = _stepnumber;
			} else {
				_count--;
			}
		}


		for(int pin = 0; pin < _pinnumber; pin++) {
			digitalWrite(_pins[pin], _stepsequence[_count][pin]);
		}
		clicks++;
		delay(_speed);
	}
	for(int pin = 0; pin < _pinnumber; pin++) {
		digitalWrite(_pins[pin], LOW);
	}
}

void Motor::fullrotation(int x) {
	int tagetstep = _completerotation * x;
	rotate(tagetstep);
}

void Motor::angle(int angle) {
	int target = angle * _anglehelper;
	rotate(target);
}


void Motor::setclockwise(bool direction) {
		if(direction) {
				_clockwise = true;
		} else if(!direction) {
				_clockwise = false;
		}
}

void Motor::setspeed(int x) {
	if( x < 2) {
			x = 2;
	}
	_speed = x;
}
