/*
Motor.h - Library for controling a Motor
Created by Dergatt
*/

#ifndef Motor_h
#define Motor_h

#include "Arduino.h"

class Motor {
	public:
		Motor(int pin1, int pin2, int pin3, int pin4, int completerotation = 2048, int speed = 2, bool halfstep = false);
		void rotate(int);
		void fullrotation(int x=1);
		void angle(int);
		void setclockwise(bool);
		void setspeed(int x=2);
	private:
		bool _clockwise;
		int _completerotation;
		int _pins[4];
		int _stepsequence[8][4];
		int _pinnumber;
		int _count;
		int _stepnumber;
		float _anglehelper;
		int _speed;

};

#endif
